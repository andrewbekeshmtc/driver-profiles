<?php
/**
 * Driver class
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author  Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */
namespace Mtc\Plugins\DriverProfiles\Classes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Driver extends Model
{
    protected $table = 'drivers';

    protected $fillable = [
        'name',
        'age',
        'bio',
        'image',
        'visible',
        'team_id',
    ];

    protected $casts = [
        'visible' => 'boolean',
    ];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * Scope a query to only include visible drivers.
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible(Builder $query)
    {
        return $query->where('visible', 1);
    }
}