<?php
/**
 * Team class
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author  Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */
namespace Mtc\Plugins\DriverProfiles\Classes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';

    protected $fillable = [
        'name',
        'bio',
        'image',
        'visible',
    ];

    protected $casts = [
        'visible' => 'boolean',
    ];

    public function drivers()
    {
        return $this->hasMany(Driver::class);
    }

    /**
     * Scope a query to only include visible teams.
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible(Builder $query)
    {
        return $query->where('visible', 1);
    }
}