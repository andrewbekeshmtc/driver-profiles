<?php
/**
 * ImageService class
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author  Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */
namespace Mtc\Plugins\DriverProfiles\Classes\Services;

class ImageService
{
    /**
     * Get the image sizes from a JSON config file
     *
     * @param bool|string $entity name of size profile
     * @param bool|string $size size to return, false for all
     * @param bool $return_path whether to return path only or whole size
     * @return string|\string[] All entities, size or a path to size for Entity image
     */
    public static function getImageSizes($entity = null, $size = false, $return_path = false)
    {
        $sizes = require dirname(dirname(__DIR__)) . '/image_sizes.php';

        if (array_key_exists($entity, $sizes)) {
            if ($size === false) {
                return $sizes[$entity];
            }

            if ($return_path === false) {
                return $sizes[$entity][$size];
            }

            return $sizes[$entity][$size]['path'];
        }

        return '';
    }
}
