<?php
/**
 * Loader file for Driver Profiles plugin
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author   Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */

if (!defined('DRIVER_PROFILES_ENABLED')) {
    include_once __DIR__ . '/admin/includes/install.php';
}
