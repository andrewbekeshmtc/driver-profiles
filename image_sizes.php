<?php

$upload_path = 'uploads/images/driverprofiles/';

return [
    'teams' => [
        'original' => [
            'path' => $upload_path . 'teams/original',
            'skip' => true,
        ],
        'small' => [
            'path' => $upload_path . 'teams/small',
            'width' => 200,
            'height' => 100,
            'crop' => 1,
        ],
        'medium' => [
            'path' => $upload_path . 'teams/medium',
            'width' => 800,
            'height' => 200,
            'crop' => 1,
        ],
    ],
    'drivers' => [
        'original' => [
            'path' => $upload_path . 'drivers/original',
            'skip' => true,
        ],
        'small' => [
            'path' => $upload_path . 'drivers/small',
            'width' => 200,
            'height' => 100,
            'crop' => 1,
        ],
        'medium' => [
            'path' => $upload_path . 'drivers/medium',
            'width' => 200,
            'height' => 400,
            'crop' => 1,
        ],
    ],
];