<?php
/**
 * Drivers listing page
 *
 * Lists all visible teams and drivers
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author   Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */
use Mtc\Plugins\DriverProfiles\Classes\Team;
use Mtc\Plugins\DriverProfiles\Classes\Services\ImageService;

$path = '../../';
require_once $path . 'core/includes/header.inc.php';

if (DRIVER_PROFILES_ENABLED === false) {
    header('Location: /');
    exit();
}

$page_meta = [
    'title' => 'Driver profiles',
    'page_title' => 'Driver Profiles',
];

$team_image_sizes = ImageService::getImageSizes('teams');
$driver_image_sizes = ImageService::getImageSizes('drivers');

$teams = Team::visible()
    ->with([
        'drivers' => function ($query) {
            $query->visible();
        }
    ])
    ->get();

// render page
$twig->display('DriverProfiles/list.twig', compact('teams', 'page_meta', 'team_image_sizes', 'driver_image_sizes'));