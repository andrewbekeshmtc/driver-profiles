# Driver Profiles

This plugin provides ability to store team and driver profiles. 

##Installation

* Clone repository into ```plugins/DriverProfiles```

```bash
git clone https://andrewbekeshmtc@bitbucket.org/andrewbekeshmtc/driver-profiles.git ~/FORK_NAME/plugins/DriverProfiles
```

* If you are using this for a project instead of development, remove .git folder

```bash
rm -rf ~/FORK_NAME/plugins/DriverProfiles/.git
```

* Add redirect rules to main .htaccess file. If a different redirect path is required this can be adjusted (see Wiki for more info)

```apacheconfig
# Driver Profiles
RewriteRule ^drivers /plugins/DriverProfiles/ [QSA,L]
```

* Open site to ensure installer script runs

* When pushing live, make sure ```plugins/DriverProfiles/admin/includes/install.php``` is opened to add necessary DB tables