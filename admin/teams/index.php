<?php
/**
 * admin/teams/index.php
 * Teams Listing page script.
 *
 * Lists all teams for driver profiles.
 * Allows deleting teams from this page.
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author   Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */
namespace Mtc\Plugins\DriverProfiles\Classes;

use Mtc\Core\PaginationTemplate;
use Mtc\Plugins\DriverProfiles\Classes\Services\ImageService;

$messages = [];
$path = '../../../../';
$page = filter_input(INPUT_GET, 'page') ?: 1;
$per_page = filter_input(INPUT_GET, 'per_page') ?: 20;
$offset = ($page - 1) * $per_page;
require_once $path . 'core/includes/header.inc.php';
$page_meta['title'] = 'Teams List';

// Delete a package
if (filter_input(INPUT_GET, 'action') === 'delete' && filter_input(INPUT_GET, 'delete_id')) {
    $delete_team = Team::find(filter_input(INPUT_GET, 'delete_id'));
    if ($delete_team) {
        $delete_team->delete();
        $messages[] = [
            'content' => 'Team deleted',
            'type' => 'success'
        ];
    }
}

$teams = Team::query()->with('drivers');

// define pagination
$pagination = new PaginationTemplate([
    'item_count' => $teams->count(),
    'per_page' => $per_page,
    'active_page' => $page,
    'link_class' => 'flink',
    'page_url' => \Util::remove_query_arg([ 'page' ]),
    'show_view_all' => false
]);

$teams = $teams->take($per_page)
    ->offset($offset)
    ->get();

// Display page
$twig->display('DriverProfiles/admin/teams/index.twig', [
    'page_meta' => $page_meta,
    'teams' => $teams,
    'thumbnail_path' => ImageService::getImageSizes('teams', 'small', true),
    'self' => $_SERVER['PHP_SELF'],
    'id' => null,
    'page' => $page,
    'messages' => $messages,
    'pagination' => $pagination->render($twig)
]);