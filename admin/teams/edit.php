<?php
/**
 * admin/teams/index.php
 * Team Create/Update page script.
 *
 * Allows managing team from this page.
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author   Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */
namespace Mtc\Modules\DriverProfiles\Classes;

use Illuminate\Support\Facades\Event;
use Mtc\Plugins\DriverProfiles\Classes\Services\ImageService;
use Mtc\Plugins\DriverProfiles\Classes\Team;

$messages = [];
$path = '../../../../';
require_once $path . 'core/includes/header.inc.php';
$page_meta['title'] = 'Team Create';

// Update package
if (filter_input(INPUT_POST, 'action') === 'update') {
    // Validate name
    if (empty(filter_input(INPUT_POST, 'name'))) {
        $messages[] = [
            'type' => 'error',
            'content' => 'Team Name is required'
        ];
    }

    // No errors
    if (empty($messages)) {
        $id = filter_input(INPUT_POST, 'id');
        $team = Team::firstOrNew([
            'id' => $id
        ]);
        $team->fill($_POST);

        if (!empty($_FILES['userfile']['size'])) {
            $file_data = upload_image('userfile', ImageService::getImageSizes('teams'));
            if (!empty($file_data['name']) && empty($file_data['error'])) {
                $team->image = $file_data['name'];
            }
        } elseif (!empty($_POST['delete_image'])) {
            $team->image = '';
        }
        $team->save();

        $_SESSION['message'] = 'Team Updated';
        // Redirect to edit page. This strips out request duplication
        header('Location: ' . $_SERVER['PHP_SELF'] . '?id=' . $team->id);
        exit();
    }
}

// Load a package
$id = filter_input(INPUT_GET, 'id');
$team = Team::firstOrNew([
    'id' => $id
]);

// Load success message if exists
if (!empty($_SESSION['message'])) {
    $messages[] = [
        'content' => $_SESSION['message'],
        'type' => 'success'
    ];
    unset($_SESSION['message']);
}

$extra_messages = array_filter(Event::fire('Teams:edit:pre-render', $team));
if (!empty($extra_messages)) {
    $messages = array_merge($messages, $extra_messages);
}

// render page
$twig->display('DriverProfiles/admin/teams/edit.twig', [
    'page_meta' => $page_meta,
    'team' => $team->toArray(),
    'self' => $_SERVER['PHP_SELF'],
    'request' => $_POST,
    'image_path' => ImageService::getImageSizes('teams', 'small', true),
    'id' => $id,
    'messages' => $messages,
]);