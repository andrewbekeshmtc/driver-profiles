<?php
/**
 * admin/drivers/index.php
 * Drivers Listing page script.
 *
 * Lists all drivers profiles.
 * Allows deleting drivers from this page.
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author   Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */
namespace Mtc\Plugins\DriverProfiles\Classes;

use Mtc\Core\PaginationTemplate;
use Mtc\Plugins\DriverProfiles\Classes\Services\ImageService;

$messages = [];
$path = '../../../../';
$page = filter_input(INPUT_GET, 'page') ?: 1;
$per_page = filter_input(INPUT_GET, 'per_page') ?: 20;
$offset = ($page - 1) * $per_page;
require_once $path . 'core/includes/header.inc.php';
$page_meta['title'] = 'Drivers List';

// Delete a package
if (filter_input(INPUT_GET, 'action') === 'delete' && filter_input(INPUT_GET, 'delete_id')) {
    $delete_driver = Driver::find(filter_input(INPUT_GET, 'delete_id'));
    if ($delete_driver) {
        $delete_driver->delete();
        $messages[] = [
            'content' => 'Driver deleted',
            'type' => 'success'
        ];
    }
}

$drivers = Driver::query()->with('team');

if (filter_input(INPUT_GET, 'list')) {
    $drivers = $drivers->where('team_id', filter_input(INPUT_GET, 'list'));
}

// define pagination
$pagination = new PaginationTemplate([
    'item_count' => $drivers->count(),
    'per_page' => $per_page,
    'active_page' => $page,
    'link_class' => 'flink',
    'page_url' => \Util::remove_query_arg([ 'page' ]),
    'show_view_all' => false
]);

$drivers = $drivers->take($per_page)
    ->offset($offset)
    ->get();

// Display page
$twig->display('DriverProfiles/admin/drivers/index.twig', [
    'page_meta' => $page_meta,
    'drivers' => $drivers->toArray(),
    'thumbnail_path' => ImageService::getImageSizes('drivers', 'small', true),
    'self' => $_SERVER['PHP_SELF'],
    'id' => null,
    'page' => $page,
    'messages' => $messages,
    'pagination' => $pagination->render($twig)
]);