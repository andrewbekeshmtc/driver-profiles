<?php
/**
 * admin/drivers/index.php
 * Driver Create/Update page script.
 *
 * Allows managing driver from this page.
 *
 * @package Mtc\Plugins\DriverProfiles
 * @author   Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */
namespace Mtc\Modules\DriverProfiles\Classes;

use Illuminate\Support\Facades\Event;
use Mtc\Plugins\DriverProfiles\Classes\Driver;
use Mtc\Plugins\DriverProfiles\Classes\Services\ImageService;
use Mtc\Plugins\DriverProfiles\Classes\Team;

$messages = [];
$path = '../../../../';
require_once $path . 'core/includes/header.inc.php';
$page_meta['title'] = 'Driver Create';

// Update package
if (filter_input(INPUT_POST, 'action') === 'update') {
    // Validate name
    if (empty(filter_input(INPUT_POST, 'name'))) {
        $messages[] = [
            'type' => 'error',
            'content' => 'Driver Name is required'
        ];
    }

    // No errors
    if (empty($messages)) {
        $id = filter_input(INPUT_POST, 'id');
        $driver = Driver::firstOrNew([
            'id' => $id
        ]);
        $driver->fill($_POST);

        if (!empty($_FILES['userfile']['size'])) {
            $file_data = upload_image('userfile', ImageService::getImageSizes('drivers'));
            if (!empty($file_data['name']) && empty($file_data['error'])) {
                $driver->image = $file_data['name'];
            }
        } elseif (!empty($_POST['delete_image'])) {
            $driver->image = '';
        }
        $driver->save();

        $_SESSION['message'] = 'Driver Updated';
        // Redirect to edit page. This strips out request duplication
        header('Location: ' . $_SERVER['PHP_SELF'] . '?id=' . $driver->id);
        exit();
    }
}

// Load a package
$id = filter_input(INPUT_GET, 'id');
$driver = Driver::firstOrNew([
    'id' => $id
]);

// Load success message if exists
if (!empty($_SESSION['message'])) {
    $messages[] = [
        'content' => $_SESSION['message'],
        'type' => 'success'
    ];
    unset($_SESSION['message']);
}

$extra_messages = array_filter(Event::fire('Teams:edit:pre-render', $team));
if (!empty($extra_messages)) {
    $messages = array_merge($messages, $extra_messages);
}

$teams = Team::all();

// render page
$twig->display('DriverProfiles/admin/drivers/edit.twig', [
    'page_meta' => $page_meta,
    'driver' => $driver->toArray(),
    'teams' => $teams->toArray(),
    'self' => $_SERVER['PHP_SELF'],
    'request' => $_POST,
    'image_path' => ImageService::getImageSizes('drivers', 'small', true),
    'id' => $id,
    'messages' => $messages,
]);