<?php
/**
 * Loader file for Driver Profiles plugin
 *
 * @category Plugins
 * @package  Mtc_Plugins
 * @author   mtc. Andrew Bekesh <andrew.bekesh@mtcmedia.co.uk>
 */

use Mtc\Core\Setting;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

if (empty($path)) {
    $path = '../../../../';
    require_once $path . 'core/includes/header.inc.php';
}

$schema = Capsule::connection()->getSchemaBuilder();

if ($schema->hasTable('teams') === false) {
    $schema->create(
        'teams',
        function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('bio');
            $table->string('image');
            $table->boolean('visible')->default(0);
            $table->timestamps();
        }
    );
}

if ($schema->hasTable('drivers') === false) {
    $schema->create(
        'drivers',
        function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedTinyInteger('age');
            $table->text('bio');
            $table->string('image');
            $table->boolean('visible')->default(0);
            $table->unsignedInteger('team_id')->index();
            $table->timestamps();
        }
    );
}

// Install site settings
$settings = [
    [
        'key' => 'DRIVER_PROFILES_ENABLED',
        'type' => 'bool',
        'value' => '1',
        'module' => 'Driver Profiles',
        'description' => 'Set true to enable Driver Profiles',
        'created_at' => new DateTime(),
    ],
];

// Install Subscription settings
foreach ($settings as $data) {
    if (!defined($data['key'])) {
        Setting::Insert($data);
    }
}

// Get the path to the admin folder without public_html / fork directory
$path_to_admin_folder = str_replace(SITE_PATH, "", dirname(__DIR__));

if (!Capsule::table("admin_menu")->where('title', '=', 'Driver Profiles')->exists()) {
    // Create the admin menu item
    $menu_id = Capsule::table("admin_menu")->insertGetId([
        "sub_id" => 0,
        "title" => "Driver Profiles",
        "path" => "",
        "activePath" => "",
        "constant" => "DRIVER_PROFILES_ENABLED",
        "icon" => "",
        "new_window" => 0,
        "order" => 4
    ]);

    Capsule::table("admin_menu")->insertGetId([
        "sub_id" => $menu_id,
        "title" => "Teams",
        "path" => $path_to_admin_folder . "/teams/",
        "activePath" => $path_to_admin_folder . "/teams/",
        "constant" => "DRIVER_PROFILES_ENABLED",
        "icon" => "fa fa-car",
        "new_window" => 0,
        "order" => 1
    ]);

    Capsule::table("admin_menu")->insertGetId([
        "sub_id" => $menu_id,
        "title" => "Drivers",
        "path" => $path_to_admin_folder . "/drivers/",
        "activePath" => $path_to_admin_folder . "/drivers/",
        "constant" => "DRIVER_PROFILES_ENABLED",
        "icon" => "fa fa-user",
        "new_window" => 0,
        "order" => 2
    ]);
}
